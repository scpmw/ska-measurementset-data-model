#!/bin/bash
OS="`uname`"
if [[ $OS == "Darwin" ]]; then
   open /Applications/Utilities/XQuartz.app
fi
xhost + ${hostname}
sudo docker run -ti --rm -e DISPLAY=$DISPLAY \
    --mount type=bind,source=/tmp/.X11-unix,target=/tmp/.X11-unix \
    --mount type=bind,source=/etc/machine-id,target=/etc/machine-id \
    --mount type=bind,source=$PWD/workspace,target=/home/developer \
    --name modelio \
    --net=host \
    --user $(id -u):$(id -g) \
    -e HOME=/home/developer \
    -e DISPLAY=${HOSTNAME}:0 \
    -w /home/developer \
    modelio > /tmp/modelio.log 2>&1

